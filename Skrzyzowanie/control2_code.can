/*@!Encoding:1250*/
/*CONTROL NODE ID    :   2 */
/*VEHLICLE LIGHTS: 1 2 5 6 */
/*PEDESTRIAN LIGHTS: 1 2 3 */

variables
{
  //-------MACROS-------//
  const int exitSuccess = -1;
  const int executionError = -2;
  
  //---PHISICAL ATTRIBUTES---//
  const int numberOfVehLights = 4;
  const int numberOfPedLights = 3;
  const int numberOfPhases = 6;
  

//  const int phaseChangeDuration = 0;
//  const int pedBlinikingTime = 0;
//  const int pedBlinkPeriod = 0;
  const int phaseChangeDuration = 3000; //ms
  const int pedBlinikingTime = 5000; //ms
  const int pedBlinkPeriod = 250; //ms
  const int errBlinkPeriod = 1000; //ms
  
  
  int phaseVehStates[numberOfPhases][numberOfVehLights] =  //State of every light in every phase
    //Order of lights as in header
  {
    {1,1,1,1}, //Phase 1
    {4,4,1,1}, //Phase 2
    {1,1,4,4}, //Phase 3
    {1,1,1,1}, //Phase 1-2 (4)
    {1,1,1,1}, //Phase 2-3 (5)
    {1,1,4,4}  //Phase 3-1 (6)
  };
  int phasePedStates[numberOfPhases][numberOfPedLights] = 
  {
    {2,1,2}, //Phase 1
    {1,2,1}, //Phase 2
    {2,2,2}, //Phase 3
    {1,1,1}, //Phase 1-2 (4)
    {1,1,1}, //Phase 2-3 (5)
    {1,1,1}  //Phase 3-1 (6)
  };
  
  
  //--MASKS USED TO CONTROL BUTTONS--//
  const int BUTTON_1 = 0x1;
  const int BUTTON_2 = 0x2;
  const int BUTTON_3 = 0x4;
  
  const int FEW_WAITING_I = 0x1;
  const int FEW_WAITING_II = 0x2;
  const int FEW_WAITING_V = 0x10;
  const int FEW_WAITING_VI = 0x20;
  
  const int MANY_WAITING_I = 0x100;
  const int MANY_WAITING_II = 0x200;
  const int MANY_WAITING_V = 0x1000;
  const int MANY_WAITING_VI = 0x2000;
  
  
  //----CURRENT STATES---//
  int currentPhase = 1;//INITIAL VALUE
  int nextPhase = 0;
  int currentVehStates[numberOfVehLights] = {1,1,1,1}; //INITIAL VALUES VEH
  int currentPedStates[numberOfPedLights] = {0,0,0};   //INITIAL VALUES PED
  int starting = 0;
  int ending = 0;
  
  int elapsedBlinkingTime = 0; //
  int errorState = 0; //When something bad happends...
  
  //Variables used for pausing simulation
  long phaseStart_time_left = -1;
  long pedBlink_time_left = -1;
  long phaseEndDelay_time_left = -1;
  long errorBlink_time_left = -1;
  
  //----TIMERS-----//
  msTimer phaseStart;
  msTimer pedBlink;
  msTimer errorBlink;
   msTimer kontroler1;
   msTimer opoznienie;
  
  //---MESSAGES---//
  message error _error;
  message phaseEnded2 phEnd;
  
  message ansWaiting ansWait;
  message ansOnCrossing onCrossing;
  message ansButton ansBut;
  //for tests
  message phaseStarted startedMsg;
      message OK2 ok2;
  
}

on start
{
 
  setTimer(kontroler1, 1000);
  
}

on timer kontroler1
{output(_error);
}

on message OK1
{
  setTimer(kontroler1, 1000);
  setTimer(opoznienie, 200) ;
}
on timer opoznienie
{
 output(ok2);
}

/*Pausing the simulation*/
on message pause 
{
  phaseStart_time_left = timeToElapse(phaseStart);
  pedBlink_time_left = timeToElapse(pedBlink);
  errorBlink_time_left = timeToElapse(errorBlink);
  
  cancelTimer(phaseStart);
  cancelTimer(pedBlink);
  cancelTimer(errorBlink);
}


on message resume 
{
  if(phaseStart_time_left != -1)
    setTimer(phaseStart, phaseStart_time_left);
  
  if(pedBlink_time_left != -1)
    setTimer(pedBlink, pedBlink_time_left);
  
  if(errorBlink_time_left != -1)
    setTimer(errorBlink, errorBlink_time_left);
}


on message startPhase
{
  if(starting)
    return;
  starting = 1;
  ending = 0;
  if(errorState) return;
  if(beginPhase(this.phase) == executionError){ 
  write("ERR %d %d", 2, 1);
    error();
  }
  
    
  //setTimer(phaseStart, phaseChangeDuration);
}

int beginPhase(int phaseToExecute)
{
  int i; 
  for(i = 0; i < numberOfVehLights; i++)
  { //Sets transition state on every Vec light 
    if(setVehLight(i, getVehTrasitionState(currentVehStates[i], phaseVehStates[phaseToExecute - 1][i])) == executionError){
      
      return executionError;
      
    }
  }

  setTimer(phaseStart, phaseChangeDuration);
  currentPhase = phaseToExecute;
  
  return exitSuccess;
}



on timer phaseStart
{
  int i;
  for(i = 0; i < numberOfVehLights; i++)
  { //Sets  state on every Vec light 
    if(setVehLight(i, phaseVehStates[currentPhase - 1][i]) == executionError)
    {
  write("ERR %d %d", 2, 2);
      error();
      return;
    }
  }
  
  for(i = 0; i < numberOfPedLights; i++)
  { //Sets transition state on every Ped light 
    if(setPedLight(i, phasePedStates[currentPhase - 1][i]) == executionError)
    {
  write("ERR %d %d", 2, 3);
      error();
      return;
    }
  }
  startedMsg.ident = 2;
  output(startedMsg);
  starting = 0;
}

on message endPhase
{  
  int i; 
  
  if(errorState) return;
  if(ending == 1)
    return;
  ending = 1;
  
  nextPhase = this.phase;
  
  //Reset all the buttons for pedestrians if the light is green
  for(i = 0; i < numberOfPedLights; i++){
    if(currentPedStates[i] == 2)
      if(resetPedButton(i) == executionError){
        return;
        error();
      }
  }
  
  
  for(i = 0; i < numberOfVehLights; i++)
  { //Sets transition state on every Vec that is green and is going to be red
    if(currentVehStates[i]== 4  && phaseVehStates[nextPhase - 1][i] == 1)
    {
      if(setVehLight(i, getVehTrasitionState(4,1)) == executionError)
      {
  write("ERR %d %d", 2, 4);
        error();
        return;
      }
    }
  }
  setTimer(pedBlink,0); // Ped blink calls next functions 
  
}

on timer pedBlink
{//Changes state of every ped light that was (according to  phasePed states)  green in current phase
 int i;
 for(i = 0; i < numberOfPedLights; i++)
 { 
    if(phasePedStates[currentPhase - 1][i] == 2 && phasePedStates[nextPhase - 1][i] == 1) //2 = GREEN 1 = RED
    {
      if(currentPedStates[i] == 2)
      {
       if(setPedLight(i,0) == executionError)
        {
  write("ERR %d %d", 2, 5);
          error();
          return;
        }
      }
      else if(currentPedStates[i] == 0)
      {
       if(setPedLight(i,2) == executionError)
        {
  write("ERR %d %d", 2, 6);
          error();
          return;
        }
      }
    }
 }
 
  
 //Sets elapsed blinking time and checks if blinking should be finished

 if(elapsedBlinkingTime < pedBlinikingTime)
 {
  elapsedBlinkingTime += pedBlinkPeriod;
  setTimer(pedBlink, pedBlinkPeriod);
 }
 else
 {
  elapsedBlinkingTime = 0;
  finishPhase();
 }
 //Sets red on vehLights in transition state (finishies the job :P ) after 

 if(elapsedBlinkingTime >= phaseChangeDuration)
 {
  finishVehRed();
 }
}

void finishVehRed()
{
//Called by blinker, changes to red lights in transition state during blinking 
  int i;
  
  for(i = 0; i < numberOfVehLights; i++)
  {
    if(phaseVehStates[nextPhase - 1][i] == 1)
    {
      if(setVehLight(i, 1) == executionError)
      {
  write("ERR %d %d", 2, 7);
        error();
        return;
      }
    }
  }
  
}
void finishPhase()
{
 int i;
 //Changes biliking ped lights to red
 for(i = 0; i < numberOfPedLights; i++)
 { 
  if(phasePedStates[currentPhase - 1][i] == 2 && phasePedStates[nextPhase - 1][i] == 1) //2 = GREEN 1 = RED
  {
    if(setPedLight(i,1) == executionError)
        {
  write("ERR %d %d", 2, 8);
          error();
          return;
        }
  }
  }
  
  output(phEnd);
  starting = 0;
}

int setVehLight(int lightNumber, int state)
//Executive function of highest order, sets specific signal to specific light (enviromental variable)
{
  if(state < 0 || state > 4) return executionError;
  if(lightNumber < 0 || lightNumber >= numberOfVehLights) return executionError;
  switch(lightNumber)
  {
    case 0:
      putValue(sigVeh_I, state);
      break;
    case 1:
      putValue(sigVeh_II, state);
      break;
    case 2:
      putValue(sigVeh_V, state);
      break;
    case 3:
      putValue(sigVeh_VI, state);
      break;
    default:
      return executionError;
  }
  currentVehStates[lightNumber] = state;
  return exitSuccess;
}


int setPedLight(int lightNumber, int state)
//Executive function of highest order, sets specific signal to specific light (enviromental variable)
{
  if(state < 0 || state > 2) return executionError;
  if(lightNumber < 0 || lightNumber > numberOfPedLights) return executionError;
  switch(lightNumber)
  {
    case 0:
      putValue(sigPed_1, state);
      break;
    case 1:
      putValue(sigPed_2, state);
      break;
    case 2:
      putValue(sigPed_3, state);
      break;
    default:
      return executionError;
  }
  
  currentPedStates[lightNumber] = state;
  return exitSuccess;
}

int getVehTrasitionState(int lastState, int nextState)
//Returns red&yellow (2) state when light changes from red to green, yellow (3) when green to red.
//When no change is going to occur returns lastState = nexState
//When Last state was 0 returns nexState.
//Returns execution error in other cases. 
{
  //Error when traisiton state (2 or 3) is passed
  if (lastState == 2 || lastState == 3) return executionError;
  else if (nextState == 2 || nextState == 3) return executionError;
  //Green to red = yellow
  else if (lastState == 4 && nextState == 1) return 3;
  //Red to green = red&yellow
  else if (lastState == 1 && nextState == 4) return 2;
  //No change
  else if (lastState == nextState) return nextState;
  //nexState when system is starting 
  else if (lastState == 0) return nextState;
  
  //otherwise
  else return executionError;
}

//HANDLING ERRORS, EMERGENCY SYSTEMS, OUR LAST HOPE

//May be called only when error occurs, cant generate errors itself 
void setAllVechLights(int state)
{
  int i;
  if(!errorState) return;
  for(i = 0; i < numberOfVehLights; i++)
      setVehLight(i, state);
}

void setAllPedLights(int state)
{
  int i;
  if(!errorState) return;
  for(i = 0; i < numberOfPedLights; i++)
      setPedLight(i, state);
}

on timer errorBlink
{
  if(currentVehStates[0] == 0) setAllVechLights(3); //All lights should be in the same state, its enough to check first one only 3 - ORANGE
  else setAllVechLights(0); //Off
  error();
}

on message error
{
  if(errorState) return;
  error();
}


//Called when an execution error occurs
void error()
{
  
  if(!errorState)
  {
    errorState = 1;
    setAllPedLights(1);
    setAllVechLights(1);
    output(_error);
  }
  
   setAllPedLights(0);
  setTimer(errorBlink, errBlinkPeriod);
}

/*
Answering to requests of control1
*/
on message reqButton
{
  if(this.ident != 2)
    return;
  
  ansBut.ident = 2;
  /*Set ansBut.val so that all the bits corresponding to the 
  pressed buttons are set to one, and to thos buttons,
  which are not pressed - set to 0.*/
  ansBut.val = (BUTTON_1 * getValue(button_1)) | (BUTTON_2 * getValue(button_2))
    | (BUTTON_3 * getValue(button_3));
  output(ansBut);
}

on message reqWaiting
{
  if(this.ident != 2)
    return;
  ansWait.ident = 2;
  
  ansWait.val = 0;
  
  if(getValue(waiting_I) == 2)
    ansWait.val |= MANY_WAITING_I;
  if(getValue(waiting_II) == 2)
    ansWait.val |= MANY_WAITING_II;
  if(getValue(waiting_V) == 2)
    ansWait.val |= MANY_WAITING_V;
  if(getValue(waiting_VI) == 2)
    ansWait.val |= MANY_WAITING_VI;
  
  if(getValue(waiting_I) == 1)
    ansWait.val |= FEW_WAITING_I;
  if(getValue(waiting_II) == 1)
    ansWait.val |= FEW_WAITING_II;
  if(getValue(waiting_V) == 1)
    ansWait.val |= FEW_WAITING_V;
  if(getValue(waiting_VI) == 1)
    ansWait.val |= FEW_WAITING_VI;
  
  output(ansWait);
}

int resetPedButton(int lightNumber){
  
  switch(lightNumber){
    case 0:
      putValue(button_1, 0);
      return exitSuccess;
    case 1:
      putValue(button_2, 0);
      return exitSuccess;
    case 2:
      putValue(button_3, 0);
      return exitSuccess;
  }
  return executionError; 
}

on message reqOnCrossing{
  onCrossing.ident = 2;
  onCrossing.val = getValue(on_crossing_3);
  output(onCrossing);
}


on message reset{
    cancelTimer(phaseStart);
  cancelTimer(pedBlink);
  cancelTimer(errorBlink);
  
  currentPhase=1;
  nextPhase = 0;
  currentVehStates[0] = 1;
  currentVehStates[1] = 1;
  currentVehStates[2] = 1;
  currentVehStates[3] = 1;
  currentPedStates[0] = 0;
  currentPedStates[1] = 0;
  currentPedStates[2] = 0;
  starting = 0;
  
  elapsedBlinkingTime = 0; //
  errorState = 0;
  phaseStart_time_left = -1;
  pedBlink_time_left = -1;
  phaseEndDelay_time_left = -1;
}